<?php session_start(); 
if ($_REQUEST['action'] == 'set_session'){
	if ($_REQUEST['session_name'] && strlen($_REQUEST['session_name']) > 1){
		if ($_REQUEST['session_value'] && strlen($_REQUEST['session_value']) > 1){
			$_SESSION[$_REQUEST['session_name']] = $_REQUEST['session_value'];
			$response = array(
				"result" => true
			);
			die(json_encode($response));
		}
	}
}
$response = array(
	"result" => false
);
echo json_encode($response);
?>
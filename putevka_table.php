<?php session_start();
include("config.php");
$query = "SELECT * FROM drivers, autos WHERE drivers.id = " . $_REQUEST['driver_id'] . " AND autos.id = " . $_REQUEST['auto_id'];
$query_2 = "SELECT * FROM ps WHERE id = " . $_SESSION['curr_ps'];
$query_3 = "SELECT * FROM putinlist WHERE id=(SELECT max(id) FROM putinlist WHERE ps_id = " . $_SESSION['curr_ps'] . ")";

$info = pg_fetch_assoc(pg_query($link, $query));
$ps = pg_fetch_assoc(pg_query($link, $query_2));
$last_putevka = pg_fetch_assoc(pg_query($link, $query_3));
if (strtotime($last_putevka['t_when']) < strtotime(date("d-m-Y")))
    $putevka_date_from = date("d-m-Y");
else
    $putevka_date_from = $last_putevka['t_when'];

if (date('A', time()) == "PM")
    $putevka_date_to = date('d-m-Y', strtotime("+1 day"));
else
    $putevka_date_to = $putevka_date_from;

?>
<h3>Печать путевки</h3>
<div class="row">
    <form class="form-horizontal" id="putevkaCreation" name="putevkaCreation">
        <input type="hidden" name="ps_id" value="<?php echo $_SESSION['curr_ps']; ?>">
        <input type="hidden" name="driver_id" value="<?php echo $_REQUEST['driver_id']; ?>">
        <input type="hidden" name="auto_id" value="<?php echo $_REQUEST['auto_id']; ?>">
        <input type="hidden" name="action" value="save_new_putevka">
        <div class="col-md-6 col-md-offset-3 selected-date-div main-screen">
            <div class="form-group">
                <label class="control-label" for="t_when">Срок действия путевки С ДАТЫ</label>
                <input type='text' class="form-control" id='putevka_date_from'
                       value="<?php echo $putevka_date_from; ?>"/>
            </div>
            <div class="form-group">
                <label class="control-label" for="t_to">Срок действия путевки ПО ДАТУ</label>
                <input type='text' class="form-control" id='putevka_date_to' value="<?php echo $putevka_date_to; ?>"/>
            </div>
            <div class="form-group">
                <label class="control-label" for="put_number">Номер путевки</label>
                <input class="form-control input-sm"  style="font-size: 20px" min="1" max="999999" step="1" type="number" id="put_number"
                       name="put_number"
                       value="<?php echo @$last_putevka['put_number'] + 1; ?>"/>
            </div>

            <div class="form-group">
                <label class="control-label" for="number">Номер автомобиля</label>
                <input readonly class="form-control input-sm" required type="text" id="number" name="number"
                       value="<?php echo @$info['number']; ?>"/>
            </div>
            <div class="form-group">
                <label class="control-label" for="marka">Марка автомобиля</label>
                <input readonly class="form-control input-sm" required type="text" id="marka" name="marka"
                       value="<?php echo @$info['marka']; ?>"/>
            </div>
            <div class="row center-block-form">
                <div class="form-group col-sm-8">
                <label class="control-label" for="name">Водитель</label>
                <input readonly class="form-control input-sm" type="text" id="name" name="name"
                       value="<?php echo @$info['name']; ?>"/>
                </div>
                <div class="form-group col-sm-4">
                <label class="control-label" for="snils">СНИЛС</label>
                <input readonly class="form-control input-sm" type="text" id="snils" name="snils"
                       value="<?php echo @$info['snils']; ?>"/>
                </div>

            </div>
            <div class="row center-block-form">
                <div class="form-group col-sm-4">
                    <label class="control-label " for="licence">Права</label>
                    <input readonly class="form-control input-sm" type="text" id="licence" name="licence"
                           value="<?php echo @$info['licence']; ?>"/>
                </div>
                <div class="form-group col-sm-4">
                    <label class="control-label " for="class">Класс</label>
                    <input readonly class="form-control input-sm" type="text" id="licence_date" name="class"
                           value="<?php echo @$info['class']; ?>"/>
                </div>
                <div class="form-group col-sm-4">
                    <label class="control-label " for="licence_date">Дата выдачи</label>
                    <input readonly class="form-control input-sm" type="text" id="licence_date" name="licence_date"
                           value="<?php echo @$info['licence_date']; ?>"/>
                </div>

            </div>



            <p>
                <button type='button' data-print-type="full" class='btn btn-success putlist-print'>
                    Печать 1 листа
                </button>
            </p>
            <p>
                <button type='button' data-print-type="duplex" class='btn btn-info putlist-print'>
                    Печать путевого листа с 2х сторон
                </button>
            </p>
        </div>
    </form>
</div>
<script>
    $('#putevka_date_from').datetimepicker({
        defaultDate: new Date(),
        minDate: '2015-08-01',
        locale: 'ru',
        viewMode: 'days',
        format: 'DD-MM-YYYY'
    });
    $('#putevka_date_to').datetimepicker({
        defaultDate: new Date(),
        minDate: '2015-08-01',
        locale: 'ru',
        viewMode: 'days',
        format: 'DD-MM-YYYY'
    });
</script>

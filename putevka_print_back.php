<!DOCTYPE HTML>
<html lang="ru-ru">
<head>
    <meta charset="utf-8">
    <title></title>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <script src="js/detect_browser.js?<?php echo rand(); ?>"></script>
    <style>@page {
            size: auto;
            margin: 0mm;
            -webkit-print-color-adjust: exact;
        }</style>
</head>
<body class="">
<img src="img/putlist_2024-2-3.png" class="absolute" style="width: 190mm; height: 270mm; margin: 8mm 0 0 8mm">
<script>
    if (detectBrowser() === 'Firefox'){
        window.print();
        window.close();
    }
</script>
</body>
</html>		
$( document ).ready(function() {
	function loadDriversTable(){
			var session_value = $('#selected-date').data('DateTimePicker').viewDate().format('YYYY-MM-DD');
			$.getJSON('set_session.php', { action: "set_session", session_name: "now_date", session_value: session_value}, function(data){
				if (!data.result) alert('Проблемы с сервером! Свяжитесь с сисадмином!');
					$( ".main.container" ).load( "driver_table.php", function() {});
			});
	}

	$(".driver-form-submit").on("click", function(){
		var prn = false;
		
		var url = "api.php";

		if ($(this).data('prn') == true){
			prn = true;
		}
		$.ajax({
			   type: "POST",
			   url: url,
			   data: $("#driverEditor").serialize(),
				dataType: 'json',
			   success: function(data)
			   {
				   console.log(data);
				   if (data.driver_id){			   
						$("#driverEditModal").modal('hide');
						loadDriversTable();						
				   }
			   }
			 });
	});
	$('[data-toggle=confirmation]').confirmation({
	  rootSelector: '[data-toggle=confirmation]',
	  onConfirm: function() {
		var url = "api.php";
		$.ajax({
			   type: "POST",
			   url: url,
			   data: 'action=delete_driver&driver_id=' + $(this).data('driver-id'),
			   success: function(data)
			   {
				   console.log(data);
				   $("#driverEditModal").modal('hide');
				   loadDriversTable();
			   }
		 });
	  },
	});
});

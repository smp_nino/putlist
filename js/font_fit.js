(function() {
  var elements = $('.font-fit');
  if(elements.length < 0) {
    return;
  }
  elements.each(function(i, element) {
    while(element.scrollWidth > element.offsetWidth || element.scrollHeight > element.offsetHeight) {
      var newFontSize = (parseFloat($(element).css('font-size').slice(0, -2)) * 0.95) + 'px';
      $(element).css('font-size', newFontSize);
    }
  });
})();
$(document).ready(function () {
    $('#selected-date').datetimepicker({
        defaultDate: new Date(),
        minDate: '2015-08-01',
        locale: 'ru',
        viewMode: 'days',
        format: 'DD-MM-YYYY'
    });
    $('#putevka_date').datetimepicker({
        defaultDate: new Date(),
        minDate: '2015-08-01',
        locale: 'ru',
        viewMode: 'days',
        format: 'DD-MM-YYYY'
    });
    $("#print-list").on("click", function (e) {
        $('.main.container').printThis({
            importCSS: true,
            loadCSS: 'css/print_table.css'
        });
    });
    $("#print-dutylist-back").on("click", function (e) {
        $(".duty-print").load("print_templates/" + $(this).data('prefix') + "/duty_list_back.php", function () {
            $('.duty-print').printThis({
                importCSS: true,
                loadCSS: 'css/print_patient_duty_list.css'
            });
        });
    });
    $("#print-all-dutylists").on("click", function (e) {
        var patients_id = Array();
        $.each($('tr[data-patient-id]'), function () {
            patients_id.push($(this).data('patient-id'));
        });
        loadDutyList(patients_id);
    });
    $('#p_datein').datetimepicker({
        defaultDate: new Date(),
        minDate: '2015-08-01',
        locale: 'ru',
        viewMode: 'days',
        format: 'DD-MM-YYYY'
    });

    $(".main.container").on('click', '.select-auto-to-print', function () {
        loadTable('drivers_select', 'auto_id=' + $(this).data('auto-id'));
    })

    $(".main.container").on('click', '.select-driver', function () {
        loadTable('putevka', 'auto_id=' + $(this).data('auto-id') + '&driver_id=' + $(this).data('driver-id'));
    })
    $("header, .main.container").on('click', '.putlist-print', function () {
        if ($(this).data('print-type') === "back"){
            var prn_window = window.open('putevka_print_back.php', '_blank', 'location=yes,height=1024px,width=1024px');
            if (detectBrowser() === 'Chrome'){
                prn_window.print();
                prn_window.addEventListener('afterprint', (event) => {
                    prn_window.close();
                });
            }
            return false;
        } else {
            var that = this;
            var url = "api.php";
            var t_when = $('#putevka_date_from').data('DateTimePicker').viewDate().format('DD-MM-YYYY');
            var t_to = $('#putevka_date_to').data('DateTimePicker').viewDate().format('DD-MM-YYYY');
            $.ajax({
                type: "POST",
                url: url,
                data: $("#putevkaCreation").serialize() + "&t_when=" + t_when + "&t_to=" + t_to,
                dataType: 'json',
                success: function (obdata) {

                    console.log(obdata);
                    let filename = ''
                    if ($(that).data('print-type') == "short") {
                        filename = 'putevka_print.php'
                    } else if ($(that).data('print-type') == "full") {
                        filename = 'putevka_print_full.php'
                    } else if ($(that).data('print-type') == "duplex") {
                        filename = 'putevka_print_full_duplex.php'
                    }
                    var print_url = filename + '?putevka_id=' + obdata.putevka_id + '&top_offset=' + obdata.top_offset + '&left_offset=' + obdata.left_offset;
                    var prn_window = window.open(print_url, '_blank', 'location=yes,height=1024px,width=1024px');


                    if (detectBrowser() === 'Chrome') {
                        prn_window.print();
                        prn_window.addEventListener('afterprint', (event) => {
                            prn_window.close();
                        });
                    }

                }
            });
        }
    })

    $(".main.container").on('click', '.set-time', function () {
        $("#time").val($(this).html());
    })

    $(".main.container").on('click', '#addNewAuto', function () {
        $("#autoEditModal .modal-content").load("auto_edit.php?action=new_auto", function () {
            $("#autoEditModal").modal('show');
        });
    });
    $("#addNewAuto").on('click', function () {
        $("#autoEditModal .modal-content").load("auto_edit.php?action=new_auto", function () {
            $("#autoEditModal").modal('show');
        });
    });
    $(".main.container").on('click', 'table.auto-table tbody tr[data-auto-id]', function () {
        $("#autoEditModal .modal-content").load("auto_edit.php?action=edit_auto&auto_id=" + $(this).data('auto-id'), function () {
            $("#autoEditModal").modal('show');
        });
    });
    $("#autoEditModal").on('hidden.bs.modal', function (e) {
        $("#autoEditModal .modal-content").html('');
    });


    $("#addNewDriver").on('click', function () {
        $("#driverEditModal .modal-content").load("driver_edit.php?action=new_driver", function () {
            $("#driverEditModal").modal('show');
        });
    });
    $(".main.container").on('click', '#addNewDriver', function () {
        $("#driverEditModal .modal-content").load("driver_edit.php?action=new_driver", function () {
            $("#driverEditModal").modal('show');
        });
    });
    $(".main.container").on('click', 'table.driver-table tbody tr[data-driver-id]', function () {
        $("#driverEditModal .modal-content").load("driver_edit.php?action=edit_driver&driver_id=" + $(this).data('driver-id'), function () {
            $("#driverEditModal").modal('show');
        });
    });

    $(".loadPage").on('click', function () {
        loadTable($(this).data('page-to-load-title'));
    });

    $("#commonSettings").on('click', function () {
        loadCommonSettings();
    });


    $("#deadStat").on('click', function () {
        $("#what_page_to_load").val('deadmeat_stats');
        loadStat();
    });


    $(".main.container").on('click', 'form#commonSettingsEditor button.settings-form-submit', function () {
        var url = "api.php";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#commonSettingsEditor").serialize(),
            success: function (data) {
                console.log(data);
                loadCommonSettings();
            }
        });
    });

    $(".stat").on('click', function () {
        $("#submenu").show();
    });

    $("li#newPutList").on('click', function () {
        $(".stat-date").hide();
        newPutList();
    });

    $(".stat-filter").on('change', function () {
        loadStat();
    });

    newPutList();

});

function newPutList() {
    var session_value = $('#selected-date').data('DateTimePicker').viewDate().format('YYYY-MM-DD');
    $.getJSON('set_session.php', {
        action: "set_session",
        session_name: "now_date",
        session_value: session_value
    }, function (data) {
        if (!data.result) alert('Проблемы с сервером! Свяжитесь с сисадмином!');
        $(".main.container").load("autos_select.php", function () {

        });
    });
}

function loadTable(table, attr) {
    var session_value = $('#selected-date').data('DateTimePicker').viewDate().format('YYYY-MM-DD');
    $.getJSON('set_session.php', {
        action: "set_session",
        session_name: "now_date",
        session_value: session_value
    }, function (data) {
        if (!data.result) alert('Проблемы с сервером! Свяжитесь с сисадмином!');
        $(".main.container").load(table + "_table.php?" + attr, function () {
        });
    });
}

function loadCommonSettings() {
    var session_value = $('#selected-date').data('DateTimePicker').viewDate().format('YYYY-MM-DD');
    $.getJSON('set_session.php', {
        action: "set_session",
        session_name: "now_date",
        session_value: session_value
    }, function (data) {
        if (!data.result) alert('Проблемы с сервером! Свяжитесь с сисадмином!');
        $(".main.container").load("common_settings.php", function () {
        });
    });
}

function loadDutyList(patient_ids) {
    var session_value = $('#selected-date').data('DateTimePicker').viewDate().format('YYYY-MM-DD');
    $.getJSON('set_session.php', {
        action: "set_session",
        session_name: "now_date",
        session_value: session_value
    }, function (data) {
        if (!data.result) alert('Проблемы с сервером! Свяжитесь с сисадмином!');
        $(".modal").modal('hide');
        $(".duty-print").load('print_templates/' + $("#prefix").val() + "/" + "duty_list.php?patient_id=" + JSON.stringify(patient_ids), function () {
            $('.duty-print').printThis({
                importCSS: true,
                loadCSS: 'css/print_patient_duty_list.css'
            });
        });
    });


}

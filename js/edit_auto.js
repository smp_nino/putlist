$( document ).ready(function() {
	function loadAutosTable(){
			var session_value = $('#selected-date').data('DateTimePicker').viewDate().format('YYYY-MM-DD');
			$.getJSON('set_session.php', { action: "set_session", session_name: "now_date", session_value: session_value}, function(data){
				if (!data.result) alert('Проблемы с сервером! Свяжитесь с сисадмином!');
					$( ".main.container" ).load( "auto_table.php", function() {});
			});
	}

	$('#number').mask("r000rr 000", {
		translation: {
				'r': {
				  pattern: /[АВЕКМНОРСТУХавекмнорстух]/,
				}
		},
		placeholder: "A000AA 000",
		}
		);

	$(".auto-form-submit").on("click", function(){
		var prn = false;
		
		var url = "api.php";

		if ($(this).data('prn') == true){
			prn = true;
		}
		$.ajax({
			   type: "POST",
			   url: url,
			   data: $("#autoEditor").serialize(),
				dataType: 'json',
			   success: function(data)
			   {
				   console.log(data);
				   if (data.auto_id){			   
						$("#autoEditModal").modal('hide');
						loadAutosTable();						
				   }
			   }
			 });
	});
	$('[data-toggle=confirmation]').confirmation({
	  rootSelector: '[data-toggle=confirmation]',
	  onConfirm: function() {
		var url = "api.php";
		$.ajax({
			   type: "POST",
			   url: url,
			   data: 'action=delete_auto&auto_id=' + $(this).data('auto-id'),
			   success: function(data)
			   {
				   console.log(data);
				   $("#autoEditModal").modal('hide');
				   loadAutosTable();
			   }
		 });
	  },
	});
});

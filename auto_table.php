<?php session_start();
include("config.php");
$query = "SELECT * FROM autos WHERE enabled = 1 AND ps = ".$_SESSION['curr_ps']." ORDER BY id ASC";

$res = pg_query($link, $query);
echo "<div class='col-lg-4 col-lg-offset-4'>";
	echo "<h3>Список автомобилей</h3>";
	echo "<table class=\"table table-striped table-condenced table-bordered auto-table\">
			<thead>
				<tr>
					<th>Номер</th>
					<th>Марка</th>
					<th>Статус</th>
				</tr>
			</thead>
			<tbody>";

	while ($row=pg_fetch_assoc($res))
	{
		if ($row['status'] == 1) {
			$status_class = "work";
			$status_name = "работает";
		} else {
			$status_class = "unwork";
			$status_name = "на ремонте";
		}
		echo "<tr class=".$status_class." data-auto-id='".$row['id']."'>";
			echo "<td>".$row['number']."</td>";
			echo "<td>".$row['marka']."</td>";
			echo "<td>".$status_name."</td>";
		echo "</tr>";
	}
	echo "</tbody></table>";
	echo "<button type='button' class='btn btn-primary' id='addNewAuto'>Добавить автомобиль</button>";
echo "</div>";
?>	
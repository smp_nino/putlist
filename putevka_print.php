<?php session_start();
include("config.php");
	$query = "SELECT * FROM putinlist, autos, drivers, ps WHERE putinlist.id = ".$_REQUEST['putevka_id']." AND ps.id = putinlist.ps_id  AND autos.id = putinlist.auto_id AND drivers.id = putinlist.driver_id";
    $info=pg_fetch_assoc(pg_query($link, $query));
	$monthes = array(
    1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
    5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
    9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря');
	$now_date = strtotime($info['t_when']);
?>
</div>
<!DOCTYPE HTML>
<html lang="ru-ru">
    <head>
        <meta charset="utf-8">
        <title></title>
		<link rel="stylesheet" href="css/print_putevka.css?<?php echo rand(); ?>">
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
        <script src="js/detect_browser.js?<?php echo rand(); ?>"></script>
		<style>@page {  size: auto;  margin: 0mm; }</style>
    </head>
    <body class="">
		<div class="absolute" style="width: 100%;">
			<div class="relative" style="width: 100%; height: 100%; top: <?php echo $info['top_offset']; ?>mm; left: <?php echo $info['left_offset']; ?>mm;">
				<div class="title" id="date" style="left: 52mm; top: 32mm">
					<div class="relative">
						<span class="absolute" style=""><?php echo date('d ', $now_date); ?></span>
						<span class="absolute" style="left: 15mm;"><?php echo $monthes[(date('n', $now_date))]; ?></span>
						<span class="absolute" style="left: 52mm;"><?php echo date(' y', $now_date); ?></span>
					</div>
				</div>
				<div class="title" id="organization" style="left: 35mm; top: 43mm">
					<?php echo $_SESSION['settings']['organization']; ?>
				</div>
				<div class="title" id="marka" style="left: 50mm; top: 59mm">
					<?php echo $info['marka']; ?>
				</div>
				<div class="title" id="number" style="left: 80mm; top: 66mm">
					<?php echo $info['number']; ?>
				</div>
				<div class="title" id="name" style="left: 30mm; top: 73mm">
					<?php echo $info['name']; ?>
				</div>
				<div class="title" id="name" style="left: 150mm; top: 73mm">
					<?php echo $info['tabid']; ?>
				</div>
				<div class="title" id="licence" style="left: 40mm; top: 82mm">
					<?php echo $info['licence']; ?>
				</div>
				<div class="title" id="licence" style="left: 140mm; top: 82mm">
					<?php echo $info['class']; ?>
				</div>
				<div class="title" id="podstation" style="left: 40mm; top: 115mm">
					<?php echo $info['short_title']; ?>
				</div>
				<div class="title" id="podstation" style="left: 40mm; top: 142mm">
					<?php echo $info['address']; ?>
				</div>
			</div>
		</div>
        <script>
            if (detectBrowser() === 'Firefox'){
                window.print();
                window.close();
            }
        </script>
    </body>
</html>		
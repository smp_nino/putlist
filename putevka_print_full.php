<?php session_start();
include("config.php");
$query = "SELECT * FROM putinlist, autos, drivers, ps WHERE putinlist.id = " . $_REQUEST['putevka_id'] . " AND ps.id = putinlist.ps_id  AND autos.id = putinlist.auto_id AND drivers.id = putinlist.driver_id";
$info = pg_fetch_assoc(pg_query($link, $query));
$monthes = array(
    1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
    5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
    9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря');
$from_date = strtotime($info['t_when']);
$to_date = strtotime($info['t_to']);

?>
</div>
<!DOCTYPE HTML>
<html lang="ru-ru">
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="css/print_putevka_long.css?<?php echo rand(); ?>">
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <script src="js/detect_browser.js?<?php echo rand(); ?>"></script>
    <style>@page {
            size: auto;
            margin: 0mm;
            -webkit-print-color-adjust: exact;
        }</style>
</head>
<body class="">
<img src="img/putlist_3-1-5.png" class="absolute" style="width: 210mm; height: 297mm; margin: 8mm 0 0 8mm">

<div class="absolute" style="width: 100%;">
    <div class="relative" style="width: 100%; height: 100%; top: 11mm; left: 15mm;">
        <div class="put_number" id="put_number" style="left: 120mm; top: 32mm">
            <div class="relative">
                <span class="absolute" style=""><?php echo str_pad($info['put_number'], 6, '0', STR_PAD_LEFT); ?></span>
                <span class="absolute" style="top: -5px; left: 23mm; font-size: 38px">*</span>
            </div>
        </div>
        <div class="title" id="date_from" style="left:11mm; top: 39mm">
            <div class="relative">
                <span class="absolute" style=""><?php echo date('d ', $from_date); ?></span>
                <span class="absolute" style="left: 24mm;"><?php echo $monthes[(date('n', $from_date))]; ?></span>
                <span class="absolute" style="left: 58mm;"><?php echo date('y', $from_date); ?></span>
            </div>
        </div>
        <div class="title" id="date_from_from" style="left:13mm; top: 49mm">
            <div class="relative">
                <span class="absolute" style=""><?php echo date('d ', $from_date); ?></span>
                <span class="absolute" style="left: 15mm;"><?php echo $monthes[(date('n', $from_date))]; ?></span>
                <span class="absolute" style="left: 42mm;"><?php echo date('y', $from_date); ?></span>
            </div>
        </div>
        <div class="title" id="date_to" style="left:75mm; top: 49mm">
            <div class="relative">
                <span class="absolute" style=""><?php echo date('d ', $to_date); ?></span>
                <span class="absolute" style="left: 16mm;"><?php echo $monthes[(date('n', $to_date))]; ?></span>
                <span class="absolute" style="left: 43mm;"><?php echo date('y', $to_date); ?></span>
            </div>
        </div>
        <div class="title" id="marka" style="left: 34mm; top: 68mm">
            <?php echo $info['marka']; ?>
        </div>
        <div class="title" id="number" style="left: 131mm; top: 68mm">
            <?php echo $info['number']; ?>
        </div>
        <div class="title" id="name" style="left: 20mm; top: 77mm">
            <?php echo $info['name']; ?>
        </div>
        <div class="title" id="tabid" style="left: 171mm; top: 83mm">
            <?php echo $info['tabid']; ?>
        </div>
        <div class="title" id="licence" style="left: 33mm; top: 89mm">
            <?php echo $info['licence']; ?>
        </div>
        <div class="title" id="class" style="left: 133mm; top: 89mm">
            <?php echo $info['class']; ?>
        </div>
        <div class="title" id="podstation" style="left: 40mm; top: 117mm">
            <?php echo $info['short_title']; ?>
        </div>
        <div class="title" id="address" style="left: 26mm; top: 130mm">
            <?php echo $info['address']; ?>
        </div>
        <div class="title" id="snils" style="left: 163mm; top: 89mm">
            <?php echo $info['snils']; ?>
        </div>
        <div class="title" id="licence_date" style="left: 86mm; top: 89mm">
            <?php echo $info['licence_date']; ?>
        </div>
    </div>
</div>
<script>
    if (detectBrowser() === 'Firefox'){
        window.print();
        window.close();
    }
</script>
</body>
</html>		
<?php session_start();
include ("config.php");

$query = "SELECT * FROM settings";
		

if (!pg_query($link, $query)) {
	echo pg_last_error();
	echo $query;
}

$row=mysqli_fetch_array(pg_query($link, $query));

?>
<div class="col-lg-6 col-lg-offset-2">
<h3>Общие настройки программы</h3>
<form class="form-horizontal" id="commonSettingsEditor" name="commonSettingsEditor">
	<input type="hidden" name="action" value="editSettings">
	<input type="hidden" name="fun" value="0">
	<div class="form-group">
		<label class="control-label col-lg-4" for="dept_title">Наименование отделения</label>
		<div class="col-lg-8">
		  <input type="text" class="form-control" id="dept_title" name="dept_title" value="<?php echo $row['dept_title']; ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-lg-4" for="dept_max_beds">Максимальное количество коек в отделении</label>
		<div class="col-lg-8">
		  <input type="number" size="2"class="form-control" id="dept_max_beds" name="dept_max_beds" value="<?php echo $row['dept_max_beds']; ?>">
		</div>
	</div>
	<!--<div class="form-group">
		<label class="control-label col-lg-4" for="fun">Приколы</label>
		<div class="col-lg-8">
			<?php /*
				if (!@$row['fun'] || @$row['fun']==0) $checked['fun'][0]="checked";
				if ( @$row['fun']==1) $checked['fun'][1]="checked";*/
				?>
			<div>
				<label><input type="radio" value="1" name="fun" <?php /*echo $checked['fun'][1]; /* ?>>Вкл</label>
				<label><input type="radio" value="0  " name="fun" <?php /*echo $checked['fun'][0]; */ ?>>Выкл</label>
			</div>
		</div>
	</div>!-->
	<button type="button" class="btn btn-primary settings-form-submit">Сохранить</button>
</form>

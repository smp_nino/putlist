<?php session_start();
include("config.php");
$query = "SELECT drivers.*, autos.number FROM drivers, autos WHERE autos.id = drivers.auto_id AND drivers.enabled = 1 AND drivers.ps = ".$_SESSION['curr_ps']." ORDER BY auto_id, name ASC;";

$res = pg_query($link, $query);
echo "<div class='col-lg-8 col-lg-offset-2'>";
	echo "<h3>Список водителей</h3>";
	echo "<table class=\"table table-striped table-condenced table-bordered driver-table\">
			<thead>
				<tr>
					<th>ФИО</th>
					<th>Водит. удостоверение</th>
					<th>Дата выдачи ВО</th>
					<th>Класс</th>
					<th>Табномер</th>
					<th>СНИЛС</th>
					<th>Машина</th>
				</tr>
			</thead>
			<tbody>";

	while ($row=pg_fetch_assoc($res))
	{
		echo "<tr data-driver-id='".$row['id']."'>";
			
			echo "<td>".$row['name']."</td>";
			echo "<td>".$row['licence']."</td>";
			echo "<td>".$row['licence_date']."</td>";
			echo "<td>".$row['class']."</td>";
			echo "<td>".$row['tabid']."</td>";
			echo "<td>".$row['snils']."</td>";
			echo "<td>".$row['number']."</td>";
		echo "</tr>";
	}
	echo "</tbody></table>";
	echo "<button type='button' class='btn btn-primary' id='addNewDriver'>Добавить водителя</button>";
echo "</div>";
?>	
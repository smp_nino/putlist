--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 10.3

-- Started on 2018-05-13 14:54:17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2158 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 188 (class 1259 OID 16401)
-- Name: autos; Type: TABLE; Schema: public; Owner: finder
--

CREATE TABLE public.autos (
    id integer NOT NULL,
    ps smallint NOT NULL,
    number text,
    brigade_number smallint,
    status smallint DEFAULT 1,
    enabled smallint DEFAULT 1
);


ALTER TABLE public.autos OWNER TO finder;

--
-- TOC entry 187 (class 1259 OID 16399)
-- Name: auto_id_seq; Type: SEQUENCE; Schema: public; Owner: finder
--

CREATE SEQUENCE public.auto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_seq OWNER TO finder;

--
-- TOC entry 2159 (class 0 OID 0)
-- Dependencies: 187
-- Name: auto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: finder
--

ALTER SEQUENCE public.auto_id_seq OWNED BY public.autos.id;


--
-- TOC entry 190 (class 1259 OID 24579)
-- Name: drivers; Type: TABLE; Schema: public; Owner: finder
--

CREATE TABLE public.drivers (
    id integer NOT NULL,
    name text,
    licence text,
    auto_id smallint,
    enabled smallint,
    ps smallint
);


ALTER TABLE public.drivers OWNER TO finder;

--
-- TOC entry 189 (class 1259 OID 24577)
-- Name: drivers_id_seq; Type: SEQUENCE; Schema: public; Owner: finder
--

CREATE SEQUENCE public.drivers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.drivers_id_seq OWNER TO finder;

--
-- TOC entry 2160 (class 0 OID 0)
-- Dependencies: 189
-- Name: drivers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: finder
--

ALTER SEQUENCE public.drivers_id_seq OWNED BY public.drivers.id;


--
-- TOC entry 186 (class 1259 OID 16393)
-- Name: ps; Type: TABLE; Schema: public; Owner: finder
--

CREATE TABLE public.ps (
    id smallint,
    title text,
    short_title text
);


ALTER TABLE public.ps OWNER TO finder;

--
-- TOC entry 185 (class 1259 OID 16387)
-- Name: settings; Type: TABLE; Schema: public; Owner: finder
--

CREATE TABLE public.settings (
    version text
);


ALTER TABLE public.settings OWNER TO finder;

--
-- TOC entry 2021 (class 2604 OID 24616)
-- Name: autos id; Type: DEFAULT; Schema: public; Owner: finder
--

ALTER TABLE ONLY public.autos ALTER COLUMN id SET DEFAULT nextval('public.auto_id_seq'::regclass);


--
-- TOC entry 2022 (class 2604 OID 24582)
-- Name: drivers id; Type: DEFAULT; Schema: public; Owner: finder
--

ALTER TABLE ONLY public.drivers ALTER COLUMN id SET DEFAULT nextval('public.drivers_id_seq'::regclass);


--
-- TOC entry 2148 (class 0 OID 16401)
-- Dependencies: 188
-- Data for Name: autos; Type: TABLE DATA; Schema: public; Owner: finder
--

INSERT INTO public.autos (id, ps, number, brigade_number, status, enabled) VALUES (18, 5, 'А111АА', 123, 1, 1);


--
-- TOC entry 2150 (class 0 OID 24579)
-- Dependencies: 190
-- Data for Name: drivers; Type: TABLE DATA; Schema: public; Owner: finder
--

INSERT INTO public.drivers (id, name, licence, auto_id, enabled, ps) VALUES (6, 'Иванов А.А.', 'фвфыв фыв фы', 18, 1, 5);


--
-- TOC entry 2146 (class 0 OID 16393)
-- Dependencies: 186
-- Data for Name: ps; Type: TABLE DATA; Schema: public; Owner: finder
--

INSERT INTO public.ps (id, title, short_title) VALUES (1, NULL, NULL);
INSERT INTO public.ps (id, title, short_title) VALUES (2, NULL, NULL);
INSERT INTO public.ps (id, title, short_title) VALUES (3, NULL, NULL);
INSERT INTO public.ps (id, title, short_title) VALUES (4, NULL, NULL);
INSERT INTO public.ps (id, title, short_title) VALUES (5, NULL, NULL);
INSERT INTO public.ps (id, title, short_title) VALUES (6, NULL, NULL);
INSERT INTO public.ps (id, title, short_title) VALUES (7, NULL, NULL);
INSERT INTO public.ps (id, title, short_title) VALUES (8, NULL, NULL);
INSERT INTO public.ps (id, title, short_title) VALUES (9, NULL, NULL);
INSERT INTO public.ps (id, title, short_title) VALUES (12, NULL, NULL);


--
-- TOC entry 2145 (class 0 OID 16387)
-- Dependencies: 185
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: finder
--

INSERT INTO public.settings (version) VALUES ('0.1');


--
-- TOC entry 2161 (class 0 OID 0)
-- Dependencies: 187
-- Name: auto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: finder
--

SELECT pg_catalog.setval('public.auto_id_seq', 18, true);


--
-- TOC entry 2162 (class 0 OID 0)
-- Dependencies: 189
-- Name: drivers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: finder
--

SELECT pg_catalog.setval('public.drivers_id_seq', 6, true);


--
-- TOC entry 2024 (class 2606 OID 24618)
-- Name: autos autos_pkey; Type: CONSTRAINT; Schema: public; Owner: finder
--

ALTER TABLE ONLY public.autos
    ADD CONSTRAINT autos_pkey PRIMARY KEY (id);


--
-- TOC entry 2026 (class 2606 OID 24593)
-- Name: drivers drivers_pkey; Type: CONSTRAINT; Schema: public; Owner: finder
--

ALTER TABLE ONLY public.drivers
    ADD CONSTRAINT drivers_pkey PRIMARY KEY (id);


--
-- TOC entry 2027 (class 2606 OID 24624)
-- Name: drivers drivers_fk; Type: FK CONSTRAINT; Schema: public; Owner: finder
--

ALTER TABLE ONLY public.drivers
    ADD CONSTRAINT drivers_fk FOREIGN KEY (auto_id) REFERENCES public.autos(id);


-- Completed on 2018-05-13 14:54:17

--
-- PostgreSQL database dump complete
--


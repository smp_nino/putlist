--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 10.3

-- Started on 2018-05-24 07:22:37

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2181 (class 1262 OID 41115)
-- Name: putinlist; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE putinlist WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';


\connect putinlist

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2183 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 41116)
-- Name: autos; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.autos (
    id integer NOT NULL,
    ps smallint NOT NULL,
    number text,
    status smallint DEFAULT 1,
    enabled smallint DEFAULT 1,
    marka text
);


--
-- TOC entry 186 (class 1259 OID 41124)
-- Name: auto_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.auto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2184 (class 0 OID 0)
-- Dependencies: 186
-- Name: auto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.auto_id_seq OWNED BY public.autos.id;


--
-- TOC entry 187 (class 1259 OID 41126)
-- Name: drivers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.drivers (
    id integer NOT NULL,
    name text,
    licence text,
    auto_id smallint,
    enabled smallint,
    ps smallint
);


--
-- TOC entry 188 (class 1259 OID 41132)
-- Name: drivers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.drivers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2185 (class 0 OID 0)
-- Dependencies: 188
-- Name: drivers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.drivers_id_seq OWNED BY public.drivers.id;


--
-- TOC entry 189 (class 1259 OID 41134)
-- Name: ps; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ps (
    id smallint,
    title text,
    short_title text,
    top_offset smallint,
    left_offset smallint,
    address text
);


--
-- TOC entry 190 (class 1259 OID 41140)
-- Name: putinlist; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.putinlist (
    id integer NOT NULL,
    auto_id smallint,
    driver_id smallint,
    t_when text,
    odometr integer,
    fuel_left smallint,
    ps_id smallint,
    now_date text DEFAULT to_char((('now'::text)::date)::timestamp with time zone, 'dd-mm-yyyy'::text) NOT NULL
);


--
-- TOC entry 191 (class 1259 OID 41147)
-- Name: putinlist_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.putinlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2186 (class 0 OID 0)
-- Dependencies: 191
-- Name: putinlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.putinlist_id_seq OWNED BY public.putinlist.id;


--
-- TOC entry 192 (class 1259 OID 41149)
-- Name: settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.settings (
    version text,
    organization text
);


--
-- TOC entry 193 (class 1259 OID 41155)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username text,
    password text,
    salt text,
    prefix text
);


--
-- TOC entry 194 (class 1259 OID 41161)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2187 (class 0 OID 0)
-- Dependencies: 194
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2035 (class 2604 OID 41163)
-- Name: autos id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.autos ALTER COLUMN id SET DEFAULT nextval('public.auto_id_seq'::regclass);


--
-- TOC entry 2036 (class 2604 OID 41164)
-- Name: drivers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.drivers ALTER COLUMN id SET DEFAULT nextval('public.drivers_id_seq'::regclass);


--
-- TOC entry 2038 (class 2604 OID 41165)
-- Name: putinlist id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.putinlist ALTER COLUMN id SET DEFAULT nextval('public.putinlist_id_seq'::regclass);


--
-- TOC entry 2039 (class 2604 OID 41166)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2166 (class 0 OID 41116)
-- Dependencies: 185
-- Data for Name: autos; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.autos (id, ps, number, status, enabled, marka) VALUES (18, 5, 'А111АА', 1, 1, 'Форд');


--
-- TOC entry 2168 (class 0 OID 41126)
-- Dependencies: 187
-- Data for Name: drivers; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.drivers (id, name, licence, auto_id, enabled, ps) VALUES (6, 'Аверьянов Владимир Евгеньевич', '2202 35482', 18, 1, 5);
INSERT INTO public.drivers (id, name, licence, auto_id, enabled, ps) VALUES (7, 'ФФФФ', '213 213 213', 18, 1, 5);


--
-- TOC entry 2170 (class 0 OID 41134)
-- Dependencies: 189
-- Data for Name: ps; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ps (id, title, short_title, top_offset, left_offset, address) VALUES (1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ps (id, title, short_title, top_offset, left_offset, address) VALUES (2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ps (id, title, short_title, top_offset, left_offset, address) VALUES (3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ps (id, title, short_title, top_offset, left_offset, address) VALUES (4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ps (id, title, short_title, top_offset, left_offset, address) VALUES (6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ps (id, title, short_title, top_offset, left_offset, address) VALUES (7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ps (id, title, short_title, top_offset, left_offset, address) VALUES (8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ps (id, title, short_title, top_offset, left_offset, address) VALUES (9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ps (id, title, short_title, top_offset, left_offset, address) VALUES (12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ps (id, title, short_title, top_offset, left_offset, address) VALUES (5, 'Нижегородская п/с', '5-я подстанция', 4, 10, 'Чачиной, 24');


--
-- TOC entry 2171 (class 0 OID 41140)
-- Dependencies: 190
-- Data for Name: putinlist; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (1, 18, 6, '08:00', 1111, 111, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (2, 18, 6, '08:00', 1111, 111, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (3, 18, 6, '08:00', 1111, 111, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (4, 18, 6, '08:00', 1111, 111, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (5, 18, 6, '08:00', 1111, 111, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (6, 18, 6, '08:00', 333, 333, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (7, 18, 6, '08:00', 333, 333, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (8, 18, 6, '08:00', 33, 444, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (9, 18, 6, '08:00', 33, 444, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (10, 18, 6, '08:00', 33, 444, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (11, 18, 6, '08:00', 213, 323, NULL, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (12, 18, 6, '08:00', 213, 323, 5, '15-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (13, 18, 6, '08:00', 222, 22, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (14, 18, 6, '08:00', 222, 22, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (15, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (16, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (17, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (18, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (19, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (20, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (21, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (22, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (23, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (24, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (25, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (26, 18, 6, '08:00', NULL, NULL, 5, '21-05-2018');
INSERT INTO public.putinlist (id, auto_id, driver_id, t_when, odometr, fuel_left, ps_id, now_date) VALUES (27, 18, 6, '20:00', NULL, NULL, 5, '21-05-2018');


--
-- TOC entry 2173 (class 0 OID 41149)
-- Dependencies: 192
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.settings (version, organization) VALUES ('0.1', 'ГБУЗ НО ССМП НН');


--
-- TOC entry 2174 (class 0 OID 41155)
-- Dependencies: 193
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.users (id, username, password, salt, prefix) VALUES (1, 'semenov', 'ccb8d0e15771c7c9a5393acaaa19e8a0', '593f9fcfc7d52', 'surg_orit');


--
-- TOC entry 2188 (class 0 OID 0)
-- Dependencies: 186
-- Name: auto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auto_id_seq', 18, true);


--
-- TOC entry 2189 (class 0 OID 0)
-- Dependencies: 188
-- Name: drivers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.drivers_id_seq', 7, true);


--
-- TOC entry 2190 (class 0 OID 0)
-- Dependencies: 191
-- Name: putinlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.putinlist_id_seq', 27, true);


--
-- TOC entry 2191 (class 0 OID 0)
-- Dependencies: 194
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- TOC entry 2041 (class 2606 OID 41168)
-- Name: autos autos_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.autos
    ADD CONSTRAINT autos_pkey PRIMARY KEY (id);


--
-- TOC entry 2043 (class 2606 OID 41170)
-- Name: drivers drivers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.drivers
    ADD CONSTRAINT drivers_pkey PRIMARY KEY (id);


--
-- TOC entry 2044 (class 1259 OID 41171)
-- Name: ps_id_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX ps_id_key ON public.ps USING btree (id);


--
-- TOC entry 2045 (class 2606 OID 41172)
-- Name: drivers drivers_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.drivers
    ADD CONSTRAINT drivers_fk FOREIGN KEY (auto_id) REFERENCES public.autos(id);


--
-- TOC entry 2046 (class 2606 OID 41177)
-- Name: putinlist putinlist_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.putinlist
    ADD CONSTRAINT putinlist_fk FOREIGN KEY (auto_id) REFERENCES public.autos(id);


--
-- TOC entry 2047 (class 2606 OID 41182)
-- Name: putinlist putinlist_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.putinlist
    ADD CONSTRAINT putinlist_fk1 FOREIGN KEY (driver_id) REFERENCES public.drivers(id);


--
-- TOC entry 2048 (class 2606 OID 41187)
-- Name: putinlist putinlist_fk2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.putinlist
    ADD CONSTRAINT putinlist_fk2 FOREIGN KEY (ps_id) REFERENCES public.ps(id);


-- Completed on 2018-05-24 07:22:37

--
-- PostgreSQL database dump complete
--


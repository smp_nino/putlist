<?php include("config.php");

if (!$_SESSION['now_date']) 
{
	$_SESSION['now_date'] = date("Y-m-d", time());
}
$res_settings = pg_query($link, "SELECT * FROM settings");
$_SESSION['settings'] = pg_fetch_assoc($res_settings);



if ($_REQUEST['reload_version']=="true"){
	setCookie('version', $_SESSION['settings']['version'], time() + 24*90*3600, '/');
	header('Location: http://'.$_SERVER['HTTP_HOST']);
}

if (!isset($_COOKIE["version"]) || $_SESSION['settings']['version'] != $_COOKIE["version"])
		$_SESSION['reload_version'] = true;
	else {
		$_SESSION['reload_version'] = false;
	}



?>
<!DOCTYPE HTML>
<html lang="ru-ru">
    <head>
        <meta charset="utf-8">
        <title>Печатаем путевки!</title>
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />		
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="assets/datetimepicker/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="assets/select2/css/select2.min.css">
        <link rel="stylesheet" href="css/style.css?<?php echo rand(); ?>">
        <link rel="stylesheet" href="css/navbar.css">
    </head>
    <body>
		<input type="hidden" id="what_page_to_load">
		<input type="hidden" id="prefix" value="<?php echo $_SESSION['prefix']; ?>">
        <header>
			<div>
				<nav class="navbar navbar-default navbar-fixed" role="navigation" id="topmenu">
				  <div class="container-fluid">
					  <ul class="nav navbar-nav navbar-left">
					  	<li style="line-height: 3;color: wheat;font-weight: bold;">
							Подстанция №<?php echo $_SESSION['curr_ps'];?>
						</li>
						<li>
							<div class="col-sm-10 selected-date-div main-screen stat-date">
								<input type='text' class="form-control" id='selected-date' />
							</div>
						</li>
						<li>
							<div class="col-sm-10 selected-date-div stat-date">
								<input type='text' class="form-control stat-date-input" id='stat-from-date' />
							</div>
						</li>
						<li>
							<div class="col-sm-10 selected-date-div stat-date">
								<input type='text' class="form-control stat-date-input" id='stat-to-date' />
							</div>
						</li>
					  </ul>
					  <ul class="nav navbar-nav navbar-right">
						<li></li>
						<li id="newPutList"><a href="#" ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> Новая путевка</a></li>
						<li class="main-screen" id="addNewDriver"><a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Новый водитель</a></li>
						<li class="main-screen" id="addNewAuto"><a href="#"><span class="glyphicon glyphicon-bed" aria-hidden="true"></span> Новый автомобиль</a></li>
                          <li class="main-screen"><a href data-print-type="back" class='putlist-print'><span
                                          class="glyphicon glyphicon-print" aria-hidden="true"></span> Печать
                                  оборота</a></li>
                          <li class="dropdown">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> Настройки <span class="caret"></span>
							</a>
						  <ul class="dropdown-menu">
							<li class="loadPage" data-page-to-load-title="driver"><a href="#">Водители</a></li>
							<li class="loadPage" data-page-to-load-title="auto"><a href="#">Автомобили</a></li>
							<!--<li id="commonSettings"><a href="#">Общие настройки</a></li>!-->
						  </ul>
						</li>
					  </ul>
				  </div>
				</nav>
			</div>
			<div>
			  <nav class="navbar navbar-default" role="navigation" id="submenu">
					<label for="m_type">
						Манипуляции в отделении:
							<select class="stat-filter manipulations" id="m_type" name="m_type" multiple="multiple">
							  <option value="AA" selected>не проводились</option>
							  <option value="BB" selected>катетеризация центральных вен</option>
							  <option value="CC" selected>интубация трахеи</option>
							  <option value="DD" selected>трахеостомия</option>
							  <option value="EE" selected>мультифильтрат</option>
							</select>
					</label>
					<label for="p_in_type">
						Дыхание пациента при поступлении:
							<select class="stat-filter breath" id="p_in_type" name="p_in_type" multiple="multiple">
							  <option value="0" selected>ИВЛ</option>
							  <option value="1" selected>Спонтанное дыхание</option>
							</select>
					</label>					
			  </nav>
			</div>
		</header>
		<div id="wrapper">
			<div class="main container">
			</div>
		</div>
        <footer></footer>
		<div class="duty-print"></div>
		<!-- Modal -->
		<div class="modal fade" id="autoEditModal" tabindex="-1" role="dialog" aria-labelledby="autoEditModal">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  
			  </div>
			</div>
		  </div>
		</div>
		<div class="modal fade" id="driverEditModal" tabindex="-1" role="dialog" aria-labelledby="driverEditModal">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  
			  </div>
			</div>
		  </div>
		</div>
		<div class="modal fade" id="deptEditModal" tabindex="-1" role="dialog" aria-labelledby="deptModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  
			  </div>
			</div>
		  </div>
		</div>	
        <script src="assets/jquery/js/jquery-3.2.0.js"></script>
        <script src="assets/bootstrap/js/bootstrap.js"></script>
        <script src="assets/printThis/js/printThis.js"></script>
        <script src="assets/moment/js/moment-with-locales.js"></script>
        <script src="assets/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
        <script src="assets/select2/js/select2.min.js"></script>
		<script src="assets/confirmation/bootstrap-confirmation.min.js"></script>
        <script src="js/detect_browser.js?<?php echo rand(); ?>"></script>
        <script src="js/main.js?<?php echo rand(); ?>"></script>
		<script src="js/font_fit.js"></script>
        <div style="position: absolute; bottom: 10px; right: 10px; font-size: 8px;">
            <?php echo $ip; ?>
        </div>
    </body>
</html>
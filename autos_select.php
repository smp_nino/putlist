<?php session_start();
include("config.php");
	$query_autos = "SELECT * FROM autos WHERE enabled = 1 AND ps = ".$_SESSION['curr_ps']." ORDER BY number, status DESC";


	if (!pg_query($link, $query_autos)) {
		echo pg_last_error();
		echo $query_autos;
	}

$res=pg_query($link, $query_autos);
?>
<h3>Выбери тачку</h3>
<?php
echo "<table class=\"table table-striped table-condenced table-bordered\">
    <thead>
    <tr>
        <th>Номер</th>
        <th>Марка</th>
        <th>Статус</th>
    </tr>
    </thead>
    <tbody>";

    while ($row=pg_fetch_assoc($res))
    {
    if ($row['status'] == 1) {
    $status_class = "work";
    $status_name = "работает";
    } else {
    $status_class = "unwork";
    $status_name = "на ремонте";
    }
    echo "<tr class='select-auto-to-print ".$status_class."' data-auto-id='".$row['id']."'>";
    echo "<td>".$row['number']."</td>";
    echo "<td>".$row['marka']."</td>";
    echo "<td>".$status_name."</td>";
    echo "</tr>";
    }
    echo "</tbody></table>";
?>

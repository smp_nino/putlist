<?php header('Content-type: text/plain; charset=utf-8');
session_start();
include "config.php";

if ($_REQUEST['action'] && $_REQUEST['action']=="save_new_putevka"){
	
	if (strlen($_REQUEST['odometr']) == 0) $_REQUEST['odometr'] = "NULL";
	if (strlen($_REQUEST['fuel_left']) == 0) $_REQUEST['fuel_left'] = "NULL";
	if (strlen($_REQUEST['top_offset']) == 0) $_REQUEST['top_offset'] = 0;
	if (strlen($_REQUEST['left_offset']) == 0) $_REQUEST['left_offset'] = 0;
	
	$query_add_putevka = "INSERT INTO putinlist ( 
	auto_id , driver_id , t_when ,  t_to , odometr , fuel_left, ps_id, put_number )
	VALUES (
	".$_REQUEST['auto_id'].", ".$_REQUEST['driver_id'].", '".$_REQUEST['t_when']."', '".$_REQUEST['t_to']."', 
    ".$_REQUEST['odometr'].", ".$_REQUEST['fuel_left'].", ".$_REQUEST['ps_id'].", ".$_REQUEST['put_number'].") RETURNING id";

	

	$query_update_ps_settings = "UPDATE ps SET top_offset = ".$_REQUEST['top_offset'].", left_offset = ".$_REQUEST['left_offset']."
						WHERE id = ".$_REQUEST['ps_id']." RETURNING id";    
    
	if (!$ps=pg_fetch_assoc(pg_query($link, $query_update_ps_settings))) {
		echo pg_last_error();
		echo $query_update_ps_settings;
	}

	$query_update_driver = "UPDATE drivers SET auto_id = ".$_REQUEST['auto_id']."
						WHERE id = ".$_REQUEST['driver_id']; 
	pg_query($link, $query_update_driver);
	
	if (!$putevka=pg_fetch_assoc(pg_query($link, $query_add_putevka))) {
		echo pg_last_error();
		echo $query_add_putevka;
	}


	$result = array("query_update_driver"=>$query_update_driver,"result"=>"saved successfuly", "putevka_id"=>$putevka['id'], "top_offset"=>$_REQUEST['top_offset'], "left_offset"=>$_REQUEST['left_offset']);
	echo json_encode($result);
}

if ($_REQUEST['action'] && $_REQUEST['action']=="save_new_auto"){
	$query_add_auto = "INSERT INTO autos ( 
	number , marka , status , enabled , ps )
	VALUES (
	'".trim(mb_strtoupper($_REQUEST['number']))."', '".$_REQUEST['marka']."', 1, 1, ".$_SESSION['curr_ps'].") RETURNING id";

	
	if (!$auto_info=pg_fetch_assoc(pg_query($link, $query_add_auto))) {
		echo pg_last_error();
		echo $query_add_auto;
	}


	$result = array("result"=>"saved successfuly", "auto_id"=>$auto_info['id']);
	echo json_encode($result);
}

if ($_REQUEST['action'] && $_REQUEST['action']=="edit_auto" && strlen($_REQUEST['auto_id'])>0){

	$query_update_auto = "UPDATE autos SET number = '".trim(mb_strtoupper($_REQUEST['number']))."', marka = '".$_REQUEST['marka']."',
						status = ".$_REQUEST['status']." WHERE id = ".$_REQUEST['auto_id']." RETURNING id";

	if (!pg_query($link, $query_update_auto)) {
		echo pg_last_error();
		echo $query_update_auto;
	}
	
	$result = array("result"=>"saved successfuly", "auto_id"=>$_REQUEST['auto_id']);
	echo json_encode($result);
}

if ($_REQUEST['action'] && ($_REQUEST['action']=="delete_auto")){
	$query = "UPDATE autos SET enabled = 0 WHERE id = ".$_REQUEST['auto_id'];
		if (!pg_query($link, $query)) {
		echo pg_last_error();
		die ($query);
	}

	die("deleted");
}

if ($_REQUEST['action'] && $_REQUEST['action']=="save_new_driver"){
	$query_add_driver = "INSERT INTO drivers ( 
	name , licence , auto_id, tabid , licence_date, snils, class , enabled , ps )
	VALUES (
	'".trim($_REQUEST['name'])."', '".trim($_REQUEST['licence'])."', ".$_REQUEST['auto_id'].",'".$_REQUEST['tabid']."','".$_REQUEST['licence_date']."','".$_REQUEST['snils']."','".$_REQUEST['class']."', 1, ".$_SESSION['curr_ps'].") RETURNING id";

	
	if (!$driver_info=pg_fetch_assoc(pg_query($link, $query_add_driver))) {
		echo pg_last_error();
		echo $query_add_driver;
	}

	$result = array("result"=>"saved successfuly", "driver_id"=>$driver_info['id']);
	echo json_encode($result);
}

if ($_REQUEST['action'] && $_REQUEST['action']=="edit_driver" && strlen($_REQUEST['driver_id'])>0){

	$query_update_driver = "UPDATE drivers SET auto_id = ".$_REQUEST['auto_id'].", name = '".trim($_REQUEST['name'])."', licence = '".$_REQUEST['licence']."', tabid = '".$_REQUEST['tabid']."' , licence_date = '".$_REQUEST['licence_date']."' , snils = '".$_REQUEST['snils']."' , class = '".$_REQUEST['class']."'
	 WHERE id = ".$_REQUEST['driver_id']." RETURNING id";

	pg_query($link, $query_update_driver);
	

	
	$result = array("result"=>"saved successfuly", "driver_id"=>$_REQUEST['driver_id']);
	echo json_encode($result);
}

if ($_REQUEST['action'] && ($_REQUEST['action']=="delete_driver")){
	$query = "UPDATE drivers SET enabled = 0 WHERE id = ".$_REQUEST['driver_id'];
		if (!pg_query($link, $query)) {
		echo pg_last_error();
		die ($query);
	}

	die("deleted");
}

if ($_REQUEST['action'] && ($_REQUEST['action']=="editSettings")){
	$query = "UPDATE settings SET dept_title='".$_REQUEST['dept_title']."', dept_max_beds=".$_REQUEST['dept_max_beds'].", fun=".$_REQUEST['fun'].";";
				
	if (!pg_query($link, $query)) {
		echo pg_last_error();
		die ($query);
	}
}

?>
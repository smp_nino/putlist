<?php
$_SESSION['curr_ps'] = 5;

$array_ips = [
    "192.168.6" => 1,
    "192.168.5" => 2,
    "192.168.9" => 3,
    "192.168.10" => 4,
    "192.168.77.2" => 5,
    "192.168.6.41" => 5,
    "192.168.0.175" => 5,
    "223.254.254" => 5,
    "223.254.254.178" => 5,
    "192.168.4" => 6,
    "192.168.2" => 7,
    "192.168.8" => 8,
    "192.168.7" => 9,
    "223.254.254.251" => 10
];

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

foreach ($array_ips as $key => $value) {
    if (strpos(strval($ip), $key) === 0) {
        $_SESSION['curr_ps'] = $array_ips[$key];
        break;
    }
}
?>
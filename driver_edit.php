<?php session_start();
include ("config.php");

if ($_REQUEST['action'] && $_REQUEST['action']=="edit_driver" && strlen($_REQUEST['driver_id'])>0){
	$modal_title="Редактирование данных о водителе";
	$modal_action="edit_driver";
	$query_drivers = "SELECT * FROM drivers WHERE enabled = 1 AND id = ".$_REQUEST['driver_id'];
			
	
	if (!pg_query($link, $query_drivers)) {
		echo pg_last_error();
		echo $query_drivers;
	}
	
	$driver_info=pg_fetch_assoc(pg_query($link, $query_drivers));

} else {
	$modal_title="Ввод данных о новом водителе";
	$modal_action="save_new_driver";
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $modal_title; ?></h4>
</div>
<form class="form-horizontal" id="driverEditor" name="driverEditor">
	<input type="hidden" name="action" value="<?php echo $modal_action; ?>">
	<input type="hidden" name="driver_id" value="<?php echo $_REQUEST['driver_id']; ?>">
	<input type="hidden" name="ps" value="<?php echo $_SESSION['curr_ps']; ?>">
	<div class="modal-body">
		<div class="col-md-6 col-md-offset-3">
			<div class="form-group">
				<label class="control-label" for="name">Фамилия И.О.</label>
				<input class="form-control input-sm" required type="text" id="name" name="name" value="<?php echo @$driver_info['name']; ?>" />
			</div>
			<div class="form-group">
				<label class="control-label" for="licence">Водительское удостоверение</label>
				<input class="form-control input-sm" required id="licence" name="licence" value="<?php echo @$driver_info['licence']; ?>" />
			</div>
			<div class="form-group">
				<label class="control-label" for="licence_date">Дата выдачи водительского удостоверения</label>
				<input class="form-control input-sm" required id="licence_date" name="licence_date" value="<?php echo @$driver_info['licence_date']; ?>" />
			</div>
			<div class="form-group">
				<label class="control-label" for="class">Класс</label>
				<input class="form-control input-sm" maxlength="5" required id="class" name="class" value="<?php echo @$driver_info['class']; ?>" />
			</div>
			<div class="form-group">
				<label class="control-label" for="tabid">Табельный номер</label>
				<input class="form-control input-sm" maxlength="10" required id="tabid" name="tabid" value="<?php echo @$driver_info['tabid']; ?>" />
			</div>
			<div class="form-group">
				<label class="control-label" for="snils">СНИЛС</label>
				<input class="form-control input-sm" maxlength="10" required id="snils" type="number" name="snils" value="<?php echo @$driver_info['snils']; ?>" />
			</div>
			<div class="form-group">
				<label class="control-label" for="auto_id">Работает на автомобиле</label>
				  <select class="form-control input-sm" id="auto_id" name="auto_id">
					<?php

						$query_autos = "SELECT * FROM autos WHERE enabled = 1 AND ps = ".$_SESSION['curr_ps']." ORDER BY id ASC";
								
						
						if (!pg_query($link, $query_autos)) {
							echo pg_last_error();
							echo $query_autos;
						}
						
						$autos_info=pg_fetch_all(pg_query($link, $query_autos));							
											
						foreach ($autos_info as $auto)
						{
							if (@$driver_info['auto_id'] == $auto['id'])
								$selected =  "selected";
							else 
								$selected = "";
							echo  "<option  value='".$auto['id']."' ".@$selected.">".$auto['number']."</option>";
						}
					?>
				  </select>
			</div>
			
		</div>
		<div class="row">
		</div>
	</div>
</form>
<div class="modal-footer">
	<?php if ($modal_action=="edit_driver") {?>
			<button class="btn btn-large btn-danger pull-left" data-driver-id="<?php echo $_REQUEST['driver_id']; ?>" data-toggle="confirmation"
				data-btn-ok-label="Удалить" data-btn-ok-icon="glyphicon glyphicon-share-alt"
				data-btn-ok-class="btn-success"
				data-btn-cancel-label="Отмена" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
				data-btn-cancel-class="btn-danger"
				data-title="Удалить запись об водителе?" data-content="Это фатально.">
		  Удалить
		</button>
	<?php } ?>
	<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
	<button type='button' class='btn btn-primary driver-form-submit'>Сохранить</button>
</div>


<script src="js/edit_driver.js"></script>
<script src="assets/maskedinput/jquery.mask.js"></script>
<script>
    $('#licence_date').datetimepicker({
        defaultDate: new Date(),
        minDate: '1950-01-01',
        locale: 'ru',
        viewMode: 'days',
        format: 'DD-MM-YYYY'
    });
</script>
<?php session_start();
include ("config.php");

if ($_REQUEST['action'] && $_REQUEST['action']=="edit_auto" && strlen($_REQUEST['auto_id'])>0){
	$modal_title="Редактирование данных об автомобиле";
	$modal_action="edit_auto";
	$query_autos = "SELECT * FROM autos WHERE enabled = 1 AND id = ".$_REQUEST['auto_id'];
			
	
	if (!pg_query($link, $query_autos)) {
		echo pg_last_error();
		echo $query_autos;
	}
	
	$auto_info=pg_fetch_assoc(pg_query($link, $query_autos));
} else {
	$modal_title="Ввод данных о новом автомобиле";
	$modal_action="save_new_auto";
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $modal_title; ?></h4>
</div>
<form class="form-horizontal" id="autoEditor" name="autoEditor">
	<input type="hidden" name="action" value="<?php echo $modal_action; ?>">
	<input type="hidden" name="auto_id" value="<?php echo $_REQUEST['auto_id']; ?>">
	<input type="hidden" name="ps" value="<?php echo $_SESSION['curr_ps']; ?>">
	<div class="modal-body">
		<div class="col-md-6 col-md-offset-3">
			<div class="form-group">
				<label class="control-label" for="number">Номер автомобиля</label>
				<input class="form-control input-sm" required type="text" id="number" name="number" value="<?php echo @$auto_info['number']; ?>" />
			</div>
			<div class="form-group">
				<label class="control-label" for="marka">Модель автомобиля</label>
				<input class="form-control input-sm" type="text" id="marka" name="marka" value="<?php echo @$auto_info['marka']; ?>" />
			</div>	
			<?php if ($modal_action=="edit_auto") {?>
				<div class="form-group">
					<div class="col-md-12 ">
						<?php 
							if (@$auto_info['status']==0) $checked['status'][0]="checked";
							if (@$auto_info['status']==1) $checked['status'][1]="checked";
						?>						
						<label class="radio-inline"><input type="radio" value="0" name="status" <?php echo $checked['status'][0];?>>на ремонте</label>
						<label class="radio-inline"><input type="radio" value="1" name="status" <?php echo $checked['status'][1];?>>работает</label>
					</div>	
				</div>
			<? } ?>
		</div>
		<div class="row">
		</div>
	</div>
</form>
<div class="modal-footer">
	<?php if ($modal_action=="edit_auto") {?>
			<button class="btn btn-large btn-danger pull-left" data-auto-id="<?php echo $_REQUEST['auto_id']; ?>" data-toggle="confirmation"
				data-btn-ok-label="Удалить" data-btn-ok-icon="glyphicon glyphicon-share-alt"
				data-btn-ok-class="btn-success"
				data-btn-cancel-label="Отмена" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
				data-btn-cancel-class="btn-danger"
				data-title="Удалить запись об автомобиле?" data-content="Это фатально.">
		  Удалить
		</button>
	<?php } ?>
	<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
	<button type='button' class='btn btn-primary auto-form-submit'>Сохранить</button>
</div>


<script src="js/edit_auto.js"></script>
<script src="assets/maskedinput/jquery.mask.js"></script>
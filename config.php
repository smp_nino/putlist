<?php
session_start();

require_once 'ip_config.php';

$db_address = getenv('DB_ADDRESS') ?:'localhost';
$db_port = getenv('DB_PORT') ?:'5432';
$db_name = getenv('DB_NAME') ?:'putinlist';
$db_user = getenv('DB_USER') ?:'postgres';
$db_password = getenv('DB_PASSWORD') ?:'';

$link = pg_connect("host=$db_address port=$db_port dbname=$db_name user=$db_user password=$db_password") or die('Could not connect: ' . pg_last_error());
